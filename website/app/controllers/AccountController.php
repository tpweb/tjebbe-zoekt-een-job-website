<?php
class AccountController extends BaseController {
	
	
	public function contact_post() {
		$validation = Validator::make(Input::all(), ['naam' => 'required', 'voornaam' => 'required','email' => 'required|email', 'bericht' => 'required']);
		$error = false;
		$errorMsg = "";
		if(!$validation->passes()) {
			$error = true;
			$errorMsg = $validation->messages();
		}

		if(!$error) {
			$userMail = Input::only('naam', 'voornaam', 'email', 'bericht');
			$userMail['bericht'] = nl2br(Input::get('bericht'));
			$data = array('bericht'=> trans('page.contact_mail_body', $userMail), 'subject'  => "Website contact");
			Mail::send('emails.mail', $data, function($message) use ($userMail) {
  				$message->from($userMail['email'], $userMail['voornaam'] . " " . $userMail['naam']);
  				$message->to("info@belwaarde.net", "Belwaarde.net")->subject("Website contact");
			});
		}
		if(strpos(Request::header('accept'), "application/json") !== false) {
			return Response::json(array('errorNr' => ($error) ? 1 : -1, 'error' => $errorMsg));
		} else {
			if($error) return Redirect::back()->withInput()->withErrors($errorMsg);
			return View::make('verzonden');
		}
	}
}