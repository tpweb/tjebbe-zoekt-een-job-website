<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function() {
	return View::make('index');
});

Route::post('/contact', function() {
	$userMail = Input::only('email', 'name', 'message', 'subject');
	$userMail['bericht'] = nl2br(Input::get('message'));
	$data = array('bericht'=> $userMail['bericht'], 'subject'  => Input::get('subject'));
	Mail::send('emails.mail', $data, function($message) use ($userMail) {
  		$message->from($userMail['email'], $userMail['name']);
  		$message->to("tjebbelievens@hotmail.com", "Tjebbe Lievens")->subject("Job:" . $userMail['subject']);
	});
	$array = array('status' => 'success', 'message' => 'Bedankt, we nemen zo snel mogelijk contact met u op.');
	return Response::json($array);
	//return View::make('contactsend');
});


Route::any('/cv', function() {
	if(Input::has('email') && strlen(Input::get('email')) > 5) {
		$userMail = Input::only('email');
		$data = array('bericht'=> $userMail['email'], 'subject'  => "TjebbeZoektEenJobCV");
		Mail::send('emails.mail', $data, function($message) use ($userMail) {
  			$message->from($userMail['email'], $userMail['email']);
  			$message->to("tjebbelievens@hotmail.com", "Tjebbe Lievens")->subject("TjebbeZoektEenJobCV");
		});
	}
	return Response::download('tjebbe_lievens_cv.pdf');
});


App::missing(function($exception) {
    return Response::view('404', array(), 404);
});

HTML::macro('menu_active', function($route, $text) {	
	if( Request::path() == $route ) {
		$active = "class = 'active'";
	}
	else {
		$active = '';
	}
 
  return '<li ' . $active . '>' . link_to($route, $text) . '</li>';
});