<?php

return array(
    'language' => 'nl',
    'toggle_navigation' => 'Toggle navigation',
    'menu_home' => 'Home',
    'menu_about' => 'Over',
    'menu_contact' => 'Contact',
    'menu_language' => 'Verander taal',
    'menu_account' => 'Account',
    'menu_change' => 'Wissel geld',
    'menu_settings' => 'Instellingen',
    'menu_cashout' => 'Vraag uitbetaling',
    'menu_logout' => 'Log uit',
    'menu_admin' => 'Admin', 
    'menu_users' => 'Gebruikers', 
    'menu_payments' => 'Uitbetalingen',
    'menu_abonement' => 'Abonnementen',
    'menu_faq' => 'FAQ',

    'master_email' => 'E-mail',
    'master_password' => 'Wachtwoord',
    'master_login' => 'Login',
    'master_register' => 'Registreer',
    'master_top' => 'Terug naar boven',
    'master_privacy' => 'privacy',
    'master_terms' => 'terms',

    //Index
    'index_titleblok' => 'Heb je soms ook heel wat geld op je sim-kaart staan waar je niets mee kan doen? Het gewoon opbellen is een beetje onnuttig en geld verspilling. Omdat ik dit probleem ook heb, bedacht ik een toffe en simpele oplossing waar mee je gratis je geld van je gsm / sim kan afhalen en terug op je bankrekening of ander betaling middel ziet verschijnen.',
    'index_registreer' => 'Registreer',

    'index_h2_werking' => 'Werking',
    'index_p_werking' => 'We hebben een dienst opgestart waar je via een betaal lijn jouw belkrediet overzet naar deze website. Jij krijgt ca. 50% van het overgezette bedrag, de telecom providers vragen ongeveer 25-30%, het bedrijf dat onze betaling verwerkt vraagt ook nog een deel van dit bedrag. 5-7% van het bedrag houden wij om de validatie sms te verzenden en om de server kosten te betalen. Wij verdienen er dus helemaal niets mee.',
    'index_view_details' => 'Lees meer',
    'index_h2_abo' => 'Eigen abonnement',
    'index_p_abo' => 'Om zoveel mogelijk te besparen hebben wij een handige tool ontwikkeld om via enkele gegevens jouw passent abonnement te laten samenstellen. Zo zal je een overzicht krijgen van de goedkoopste providers passend bij uw verbruik.',
    'index_h2_opgelet' => 'Opgelet!',
    'index_p_opgelet' => 'Om dat deze dienst werkt via een telefonische overdracht is het voor u zeer belangrijk dat u geen abonnement hebt maar een prepaid sim kaart. Bij een abonnement zullen deze kosten bij op je maandelijkse abonnement kost worden opgeteld, dit kan je wel eens duur komen te staan.',
    
    'index_r_create_account' => 'Maak een account',
    'index_r_email' => 'E-mail',
    'index_r_password' => 'Wachtwoord',
    'index_r_firstname' => 'Voornaam',
    'index_r_name' => 'Naam',
    'index_r_register' => 'Registreer',
    'index_r_success' => 'Registratie voltooid!',
    'index_r_failed' => 'Registratie mislukt.',

    'index_bruikbaar' => 'Bruikbaar in:',

    'index_t_title' => '',
    'index_t_description' => 'Met belwaarde.net is het mogelijk om je ongebruikte belkrediet om te zetten naar echt geld op je bankrekening of paypal. Of om een snelle berekening te maken welk abonnement het best bij jouw past.',
    'index_t_keywords' => 'belwaarde, geld, omzetten, belwaarde, belkrediet, abonnementen.',

    'custom_title' => 'Passend abonnement berekenen',
    'custom_header' => 'Er zijn 100den verschillende mogelijkheden van abonnement, elk heeft zijn eigen kenmerk maar ook zijn prijs. Je hebt zoveel keuzes dat het zeer moeilijk is om het beste voor jouw te kiezen. Via deze webpagina willen we die keuze makkelijker maken. We berekenen de verwachte gemaakte kosten en zo kiezen we het best bijpassend abonnement.',
    'custom_form_title' => 'Bereken passend abonnement',
    'custom_bellen' => 'Aantal verbruikte belminuten per maand:',
    'custom_smsen' => "Aantal verbruikte SMS'jes per maand:",
    'custom_internet' => "Aantal verbruikte internet MB's per maand:",
    'custom_btn' => "Bereken",
    'custom_merk' => 'Merk',
    'custom_naam' => 'Naam',
    'custom_sms' => "SMS'en",
    'custom_bel' => 'Belminuten',
    'custom_internet' => 'Internet',
    'custom_country' => 'Land',
    'custom_prijs' => 'Prijs / Verwachting',
    'custom_ongelimiteerd' => 'ongelimiteerd',
    'custom_niet_bruikbaar' => "Deze type's zijn NIET bruikbaar in ons systeem.",
    'custom_wel_bruikbaar' => "Deze type's zijn WEL bruikbaar in ons systeem.",
    'custom_t_title' => ' | Custom',
    'custom_t_description' => 'Bereken makkelijk en snel je best passende telecom abonnement. Betaal niet meer te veel, en zoek het goedkoopste voor jouw behoeften.',
    'custom_t_keywords' => 'belwaarde, belkrediet, abonnementen, zoeken, telecom',
    

    //about
    'about_h2' => 'Over',
    'about_p' => 'Belwaarde.net is een website ontworpen door de TPWeb.org. Wij vonden het steeds vervelend om ongebruikte belwaarde op mobiele toestellen verloren te zien gaan. Hier door bedachten we iets waarmee we ons ongebruikt beltegoed terug konden omzetten naar geld. Na lang testen hebben wij besloten dit systeem voor iedereen toegankelijk te maken. Door dat we het systeem goed getest hebben zouden er normaal niet te veel kinder ziektes in mogen zitten. Je kan je misschien ook afvragen waarom je maar 50% krijgt? Wel dat is zeer eenvoudig wij verdienen er niets mee. Wij hebben zelf niet de financiële mogelijkheden om zelf een sms server te kopen of huren, deze toepassing wordt mogelijk gemaakt door verschillende andere bedrijven die elk hun deel van het bedrag afhouden voor ons blijft er nog geen 5% van het bedrag over en is het dus al niet makkelijk om de server kosten van de website te betalen. Rijk zullen wij er niet van worden, maar door gebruik te maken van het systeem hebben we al heel wat geld uitgespaard.',
    'about_t_title' => ' | Over',
    'about_t_description' => 'Wil je meer weten over belwaarde.net, wat we doen, waarom en hoe. Dan kan je op deze pagina terecht.',
    'about_t_keywords' => 'belwaarde, belkrediet, abonnementen, zoeken, telecom, over',
    
    'account_title' => 'Welkom :name,',
    'account_head_1' => 'Welkom op deze pagina, u hebt genoeg geld op je account om dit te kunnen omzetten! Via de <a href="' . LaravelLocalization::localizeURL('/account/cashout') . '">cashout</a> pagina kan je het behaalde bedrag van &euro; :amount omzetten en laten uitbetalen.',
    'account_head_2' => 'Welkom op deze pagina, u hebt nog niet genoeg geld op je account om dit te kunnen omzetten. Via <a href="' . LaravelLocalization::localizeURL('/account/change'). '">deze</a> pagina kan je geld van je gsm afhalen. Om het later om te zetten.',
    'account_gespaard_bedrag' => 'Gespaard bedrag:',
    'account_totaal' => 'Totaal gespaard bedrag:',
    'account_uitbetaald' => 'Uitbetaald bedrag:',
    'account_lopend' => 'Lopende uitbetaling:',
    'account_transacties' => 'Transacties:',
    'account_ref' => 'Vrienden uitnodigen:',
    'account_ref_title' => 'Jouw vrienden',
    'account_ref_name' => 'Naam',
    'account_ref_commision' => 'Jouw winst',

    'account_zetgeldom' => 'Zet geld om',
    'account_requestpayout' => 'Vraag uitbetaling',
    'account_settings' => 'Instellingen',

    'account_edit' => 'Wijzig gegevens',
    'account_edit_save' => 'Opslaan',
    'account_edit_saved' => 'Gegevens goed opgeslagen.',

    'account_t_title' => ' | Account',
    'account_t_description' => 'Account',
    'account_t_keywords' => 'belwaarde, belkrediet, abonnementen, zoeken, telecom, account',


    'cashout_title' => 'Uitbetaling',
    'cashout_heading' => 'Via deze pagina is het mogelijk om een uitbetaling aan te vragen. Afhankelijk van de methode zal de overdracht direct tot max. 14dagen duren.',
    'cashout_chose_method' => 'Kies methode:',
    'cashout_chose_amount' => 'Kies Bedrag:',
    'cashout_paypal' => 'Paypal e-mail:',
    'cashout_iban' => 'IBAN:',
    'cashout_paypal_placeholder' => 'Je paypal e-mail, example@example.com',
    'cashout_iban_placeholder' => 'BEXX XXXX XXXX XXXX',
    'cashout_email' => 'Je e-mail:',
    'cashout_email_placeholder' => 'Je e-mail (hier versturen we de verdere informatie)',
    'cashout_ga_verder' => 'Ga verder',
    'cashout_1_h2' => 'Kies je methode',
    'cashout_1_p' => 'Momenteel zijn we nog beperkt in de verschillende mogelijkheden om een uitbetaling te verwerken. We proberen in de komende maanden ons aanbod uit te breiden.',
    'cashout_2_h2' => 'Controleer gegevens.',
    'cashout_2_p' => 'Het is zeer belangrijk dat u de juiste gegevens doorgeeft. Controleer ze dus zeker!',
    'cashout_3_h2' => 'Verwerking',
    'cashout_3_p' => 'Afhankelijk van de methode en het bedrag is het mogelijk om een uitbetaling direct of tot max 14 dagen te verwerken.',
    'cashout_view_details' => 'Lees meer',

    'cashout_success' => 'Betaling zal verwerkt worden!',
    'cashout_failed' => 'Het gekozen bedrag is niet mogelijk.',
    'cashout_minimum_error' => 'U dient minstens &euro; :amount te halen.',

    'cashout_hst_title' => 'Uitbetalings geschiedenis',
    'cashout_hst_bedrag' => 'Bedrag',
    'cashout_hst_methode' => 'Methode',
    'cashout_hst_datum' => 'Datum',
    'cashout_hst_datum_end' => 'Verwerking',
    'cashout_hst_gegevens' => 'Gegevens',
    'cashout_hst_no' => 'U hebt nog geen uitbetalingen aangevraagd.',
    'cashout_paypal' => 'Paypal',
    'cashout_bank' => 'Bankoverschrijving',
    'cashout_mv' => 'Mobilevikings.be',
    'cashout_proximus' => 'Proximus.be',
    'cashout_mobistar' => 'Mobistar.be',
    'cashout_base' => 'Base.be',

    'cashout_t_title' => ' | Uitbetaling',
    'cashout_t_description' => 'uitbetalingen',
    'cashout_t_keywords' => 'uitbetaling',


    'change_title' => 'Zet geld om',
    'change_heading' => 'Via deze pagina is het mogelijk om je belwaarde van je gsm af te halen. Een deel van dat bedrag zal op je account worden bijgeteld, hierna kan je dit laten uitbetalen. Je kiest hieronder het land waar u zich bevind. U krijgt een telefoon nummer en een unieke code. U belt het telefoon nummer en als men vraagt naar u code geeft u deze in. Hierna zal het ingestelde bedrag van je telefoon gaan en bij je account bij komen. U kan dit meerdere keren herhalen.',
    'change_chose_country' => 'Kies land:',
    'change_next' => 'Ga verder',
    'change_chose_amount' => 'Kies bedrag:',
    'change_tel_nr' => 'Telefoon nr:',
    'change_code' => 'Code:',
    'change_amount' => 'Bedrag:',
    'change_info' => 'Info:',
    'change_controle' => 'Controle:',
    'change_1_h2' => 'Kies je land',
    'change_1_p' => 'Omdat we maar een beperkt aantal landen kunnen aanbieden en niet elk land dezelfde telefoon nummer heeft is het zeer belangrijk om het je juiste land te selecteren.',
    'change_2_h2' => 'Bellen',
    'change_2_p' => 'U krijgt een code en telefoon nummer, u belt dit nummer met je mobiel toestel en als men de code vraagt geeft u deze door. Hierna zal het ingestelde bedrag van je telefoon gaan.',
    'change_3_h2' => 'Opgelet!',
    'change_3_p' => 'Om dat deze dienst werkt via een telefonische overdracht is het voor u zeer belangrijk dat u geen abonnement hebt maar een prepaid sim kaart. Bij een abonnement zullen deze kosten bij op je maandelijkse abonnements kost worden opgeteld, dit kan je wel eens duur komen te staan.',
    'change_read_more' => 'Lees meer',

    'change_info_cpg' => "&euro; :number per gesprek",
    'change_info_cpm' => "&euro; :number per minuut, c.a. :duration seconden",

    'change_hst_title' => "Transactie geschiedenis",
    'change_hst_code' => 'Code',
    'change_hst_bedrag' => 'Bedrag',
    'change_amount_get' => 'U ontvangt: :payout',
    'change_hst_ontvangen_bedrag' => 'Ontvangen',
    'change_hst_datum' => 'Datum',
    'change_hst_no' => 'U hebt nog geen transactie geschiedenis.',

    'change_t_title' => ' | Omzetting',
    'change_t_description' => 'omzetten',
    'change_t_keywords' => 'omzetten',


    'contact_title' => 'Contact',
    'contact_name' => 'Naam: ',
    'contact_firstname' => 'Voornaam: ',
    'contact_email' => 'E-mail: ',
    'contact_message' => 'Bericht: ',
    'contact_send' => 'Verzend',
    'contact_send_title' => 'Uw mail is succesvol verzonden!',
    'contact_send_heading' => 'Wij hebben uw mail succesvol ontvangen. Wij zullen dan ook zo spoedig mogelijk proberen te reageren!',
    'contact_send_btn' => 'Verder',

    'contact_t_title' => ' | Contact',
    'contact_t_description' => 'Vragen of opmerkingen over het omzetten van belkrediet en of belwaarde of over het opzoeken van passende abonnementen.',
    'contact_t_keywords' => 'belwaarde, belkrediet, abonnementen, zoeken, telecom, contact, omzetten, vragen',
    'contact_mail_body' => 'Naam: :naam <br>Voornaam: :voornaam <br> e-mail: :email <br> Bericht: <br> :bericht',


    'login_title' => 'Login',
    'login_success' => 'Login is gelukt!',
    'login_email' => 'E-mail:',
    'login_password' => 'Wachtwoord:',
    'login_email_placeholder' => 'E-mail',
    'login_password_placeholder' => 'Wachtwoord',
    'login_btn' => 'Login',
    'login_lost_password' => 'Wachtwoord vergeten?',
    'login_failed' => 'E-mail of wachtwoord is fout.',

    'login_t_title' => ' | Login',
    'login_t_description' => 'Met belwaarde.net is het mogelijk om je ongebruikte belkrediet om te zetten naar echt geld op je bankrekening of paypal. Of om een snelle berekening te maken welk abonnement het best bij jouw past. Via deze webpagina is het mogelijk om je in te loggen bij belwaarde.net.',
    'login_t_keywords' => 'belwaarde, login, account, geld omzetten',

    'register_title' => 'Registreer',
    'register_success' => 'Registratie is gelukt!',

    'register_t_title' => ' | Registreer',
    'register_t_description' => 'Met belwaarde.net is het mogelijk om je ongebruikte belkrediet om te zetten naar echt geld op je bankrekening of paypal. Of om een snelle berekening te maken welk abonnement het best bij jouw past. Maak een account op belwaarde.net en zet onmiddellijk je belkrediet om.',
    'register_t_keywords' => 'belwaarde, login, account, geld omzetten, register',
    'register_mail_subject' => "Registratie op Belwaarde.net",
    'register_mail_body' => 'Beste :name,<br> De registratie op Belwaarde.net werd voltooid! U kan vanaf nu ten volle genieten van de extra diensten op belwaarde.net. Binnenkort zullen er nog meer diensten gelanceerd worden hou dus zeker de website in de gaten!<br><br>Mvg,<br> Belwaarde.net',

    'remind_title' => 'Wachtwoord vergeten',
    'remind_btn' => 'Verzend',
    'remind_success' => 'Er werd een mail verzonden met de verdere instructies.',
    'remind_invalid_token' => 'De token is niet geldig.',
    'remind_user' => 'De gegevens kloppen niet',


    'faq_t_title' => ' | FAQ',
    'faq_t_keywords' => 'belwaarde, geld omzetten, vragen, faq',
    'faq_t_description' => 'Met belwaarde.net is het mogelijk om je ongebruikte belkrediet om te zetten naar echt geld op je bankrekening of paypal. Of om een snelle berekening te maken welk abonnement het best bij jouw past. Dit gebeurd op een methode die we zelf bedacht hebben, het is dan ook van zelf sprekend dat je hier vragen over hebt. Op deze FAQ pagina kan je al heel wat vragen vinden over onze website.',
    'faq_title' => 'FAQ',
    'faq_header' => 'Met belwaarde.net is het mogelijk om je ongebruikte belkrediet om te zetten naar echt geld op je bankrekening of paypal. Dit gebeurd op een methode die we zelf bedacht hebben, het is dan ook van zelf sprekend dat je hier vragen over hebt. Op deze FAQ pagina kan je al heel wat vragen vinden over onze website.',
    'faq_no_result' => 'Geen resultaten gevonden.',

    'news_t_title' => ' | Nieuws',
    'news_t_keywords' => 'belwaarde, geld omzetten, nieuws',
    'news_t_description' => 'Met belwaarde.net is het mogelijk om je ongebruikte belkrediet om te zetten naar echt geld op je bankrekening of paypal. Of om een snelle berekening te maken welk abonnement het best bij jouw past. Door dat we steeds aan dit concept blijven werken willen we u graag op de hoogte houden van onze updates.',
    'news_title' => 'Nieuws',
    'news_header' => 'Met belwaarde.net is het mogelijk om je ongebruikte belkrediet om te zetten naar echt geld op je bankrekening of paypal. Door dat we steeds aan dit concept blijven werken willen we u graag op de hoogte houden van onze updates.',
    'news_no_result' => 'Geen resultaten gevonden.',

    'privacy_t_title' => ' | Privacy',
    'privacy_t_keywords' => 'belwaarde, geld omzetten, privacy',
    'privacy_t_description' => 'Met belwaarde.net is het mogelijk om je ongebruikte belkrediet om te zetten naar echt geld op je bankrekening of paypal. Of om een snelle berekening te maken welk abonnement het best bij jouw past. De privacy van onze gebruikers is zeer belangrijk, hier leest u hier meer over.',
    'privacy_title' => 'Privacy',
    'privacy_header' => '',
    'privacy_tekst' => "<ol>
    <li><h3>Gegevens van bezoekers</h3>
<p>We houden geen gegevens van bezoekers bij, enkel de gegevens zoals naam, voornaam e-mail en een gesalt wachtwoord. Ook houden we al de transacties bij net zoals de uitbetalingen. Belwaarde.net zorgt er voor dat al de gegevens met de nodige beveiliging opgeslagen worden.</p>
</li>
<li><h3>Cookies</h3>
<p>Cookies zijn kleine tekstbestanden die door een pagina van de website op de computer van de bezoeker worden geplaatst. In zo'n cookie wordt informatie opgeslagen zoals bepaalde voorkeuren van de bezoeker. Daardoor kunnen we de bezoeker bij een volgend bezoek nog beter van dienst zijn. De bezoeker kan zelf bepalen hoe er met functionele cookies omgegaan moet worden. Hij kan zijn browser zo instellen dat die het gebruik van functionele cookies toestaat, niet toestaat of gedeeltelijk toestaat. In dit laatste geval kan worden ingesteld welke websites functionele cookies mogen plaatsen. Bij alle overige websites wordt het dan verboden. Deze mogelijkheid wordt door de meestgebruikte moderne browsers geboden. Cookies kunnen altijd van een computer worden verwijderd, ook weer via de browser.</p>
<p>Belwaarde.net maakt zelf niet gebruik van cookies enkel, bij het uitnodigen van vrienden wordt er een cookie tijdelijk bewaart.</p> 
<p>Bekwaarde.net kan gebruik maken van Analytics cookies waarmee niet het surfgedrag van individuen maar van grote aantallen bezoekers - geanonimiseerd - worden verwerkt tot grafieken en patronen die ons helpen om onze websites te verbeteren en te optimaliseren.</p>
</li>
<li><h3>Vragen</h3>
<p>Bezoekers kunnen met hun vragen over deze Privacy Policy terecht bij belwaarde.net, we hebben een contact pagina voorzien op deze website hiervoor.</p>
</li>
<li>
<h3>Disclaimer</h3>
<p>Belwaarde.net is gerechtigd de inhoud van de Privacy Policy te wijzigen zonder dat de bezoeker daarvan op de hoogte wordt gesteld. Het doorvoeren van de wijziging op de website is daarvoor afdoende.</p>
</li></ol>",


    'terms_t_title' => ' | Voorwaarden',
    'terms_t_keywords' => 'belwaarde, geld omzetten, terms, voorwaarden',
    'terms_t_description' => 'Met belwaarde.net is het mogelijk om je ongebruikte belkrediet om te zetten naar echt geld op je bankrekening of paypal. Of om een snelle berekening te maken welk abonnement het best bij jouw past. Aan ons systeem zijn ook enkele voorwaarden verbonden, hier leest u hier meer over.',
    'terms_title' => 'Voorwaarden',
    'terms_header' => '',
    'terms_tekst' => "<ol>
    <li><h3>Algemeen</h3>
    <p>Als belwaarde.net ooit zal stoppen, worden de tegoeden van de gebruikers met een minimaal openstaand tegoed van € 2,00 volledig uitbetaald, tenzij dit door bepaalde instanties onmogelijk wordt gemaakt.</p>
    <p>De gebruiker dient zichzelf op de hoogte te stellen van de voorwaarden van zijn/haar provider met betrekking tot beltegoed cashen.</p>
    </li>
    <li><h3>Website</h3>
    <p>belwaarde.net is niet aansprakelijk voor eventuele fouten in de website of foutief verstrekte gegevens en/of informatie. De gebruiker kan een e-mail naar de beheerder sturen om deze op de hoogte te stellen van de eventuele fout, en de beheerder zal de fout dan zo spoedig mogelijk proberen te herstellen.</p>
    <p>belwaarde.net kan op geen enkele manier aansprakelijk worden gesteld voor (te) hoge rekeningen door gebruik of misbruik van de op de website beschikbaar gestelde diensten.</p>
    </li>
    <li><h3>Account en uitbetalingen</h3>
    <p>Per persoon is één account toegestaan. Bij controle kunnen de meerdere accounts worden verwijderd. Meerdere telefoonnummers per account is wel toegestaan.</p>
    <p>Alle gegevens dienen naar waarheid te worden ingevuld.</p>
    <p>Het is niet toegestaan om gebruik te maken van de door de website aangeboden diensten met een vaste- of buitenlandse telefoonlijn.</p>
    <p>Als er zich een geval van fraude, misbruik of enige activiteit die door belwaarde.net of door de telecomprovider als onbehoorlijk of illegaal wordt verondersteld, ontdekt wordt, is het mogelijk dat het openstaande tegoed niet uitbetaald wordt en dat de gebruiker wordt geblokkeerd voor verder gebruik van de diensten. Eventueel kunnen er juridische stappen worden ondernomen.</p>
    <p>Normaliter wordt er uitbetaald binnen veertien dagen. Mocht de telecomprovider meer onderzoek willen doen over de gemaakte gesprekken, mochten er verdachte activiteiten op de website plaatsvinden of verzoekt de provider om een andere reden te wachten met de uitbetaling naar de klant toe, dan kan de uitbetaling worden uitgesteld, totdat duidelijk is of het tegoed definitief aan de klant uitbetaald mag worden of dat het bedrag mogelijk nog teruggevorderd wordt of terugbetaald dient te worden.</p>
    <p>Wilt de telecomprovider om een bepaalde reden de uitbetaling terugvorderen of niet aan de klant uitkeren, dan kan het openstaande tegoed niet aan de gebruiker worden uitgekeerd.</p>
    <p>De dienst is enkel bedoeld om voor overgebleven beltegoed geld terug te krijgen. Het is dan ook niet toegestaan om te bellen met een aansluiting zonder beltegoed (maar enkel belminuten) of een aansluiting die pas net in gebruik is.</p>
    </li>
    <li><h3>Privacy</h3>
    <p>Je gegevens kunnen gebruikt/doorgegeven worden om eventuele fraude/misbruik op te sporen en/of aan te geven of om eventuele geldende wet- en regelgeving, wettelijke procedure of een afdwingbaar verzoek van een officiële instantie na te komen. In overige gevallen worden je gegevens onder geen enkele voorwaarde misbruikt, verkocht of doorgegeven aan derden.</p>
    </li>
    </ol>",


    '404_title' => 'Error, Pagina niet gevonden.',
    '404_header' => 'Pagina niet gevonden.',
    '404_t_title' => ' | Pagina niet gevonden',
    '404_t_description' => 'Met belwaarde.net is het mogelijk om je ongebruikte belkrediet om te zetten naar echt geld op je bankrekening of paypal. Of om een snelle berekening te maken welk abonnement het best bij jouw past. Pagina niet gevonden.',
    '404_t_keywords' => 'belwaarde, login, account, convert, page not found, pagina niet gevonden.',
);