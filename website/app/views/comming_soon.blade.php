<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Tjebbe Lievens</title>
    <meta name="description" content="">

    <!-- Bootstrap -->
    {{ HTML::style('https://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css') }}
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,800,700,300,600,400italic&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    {{ HTML::style('css/main.css') }}
    {{ HTML::style('css/font-awesome.min.css') }}

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-48904183-5', 'tpweb.org');
  ga('send', 'pageview');

</script>
  </head>
  <body>
    <div id="home" class="scrollspy parallax">
      <div id="first-text">
        <h1>Comming soon!</h1>
        <h2>Deze webpagina is nog in opbouw!</h2>
      </div>
    </div>
    <div class="navbar navbar-bottom" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#home">Tjebbe zoekt een job</a>
        </div>
        <div class="navbar-collapse collapse pull-right">
          <ul class="nav navbar-nav">
            <li class="active"><a href="#home">Home</a></li>
          </ul>
        </div>
      </div>
    </div>
    
    

    <footer>
      <div class="copyright-block clearfix">
        <div class="container">
          <div class="pull-left">
            <p class="pull-left">Copyright © 2014 by <a href="http://www.tpweb.org" target="_blank">Tjebbe Lievens</a>. All Rights Reserved</p>
          </div>  
          <div class="pull-right">
          </div>  
        </div>
      </div>  
    </footer>


    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="{{ URL::asset('js/jquery-1.10.2.min.js') }}"><\/script>')</script>
    <script src="{{ URL::asset('http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('js/main.js') }}"></script>
  </body>
</html>