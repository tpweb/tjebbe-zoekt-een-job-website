<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title></title>
        <style></style>
    </head>
    <body style="margin: 0; padding: 0;">
        <div style="width: 80%; background-color:#e7222c; min-height: 40px; margin: 0; padding: 10px 10% 0 10%; color: #FFF;">
            <h1><a href="http://belwaarde.net" style="color: #FFF; text-decoration: none;">Belwaarde.net</a></h1>
        </div>
        <div style="width: 80%; min-height: 200px; margin: 0; padding: 0 10% 0 10%;">
        To reset your password, complete this form: <a href="{{ URL::to('password/reset', array($token)) }}">{{ URL::to('password/reset', array($token)) }}</a>.
        </div>
        <div  style="width: 80%; background-color:#ffbbbc; min-height: 40px; margin: 0; padding: 10px 10% 0 10%;">
&copy; Belwaarde.net
        </div>
    </body>
</html>