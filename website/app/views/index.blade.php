<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Tjebbe Lievens</title>
    <meta name="description" content="">

    <!-- Bootstrap -->
    {{ HTML::style('https://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css') }}
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,800,700,300,600,400italic&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    {{ HTML::style('css/main.css') }}
    {{ HTML::style('css/font-awesome.min.css') }}
    {{ HTML::style('css/font-awesome.min 2.css') }}
    {{ HTML::style('https://rawgit.com/lucaong/jQCloud/master/jqcloud/jqcloud.css') }}

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
    .score-disable {
      display: none;
    }
    .score-enable {
      display: inline;
    }
    </style>
    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-48904183-5', 'tpweb.org');
  ga('send', 'pageview');

</script>
  </head>
  <body>
    <div id="home" class="scrollspy parallax">
      <div id="third-text">
        <h1>But please, take 5 minutes and find out why</h1>
        <h2 class="color">I am not</h2>
      </div>
      <div id="first-text">
        <h1>My name is Tjebbe Lievens</h1>
        <h2>You are thinking ...</h2>
      </div>
      <div id="second-text">
        <h2>Just another</h2>
        <h1>ICT Graduate.</h1>
      </div>
    </div>
    <div class="navbar navbar-bottom" role="navigation">
      <div class="container">
        <div class="navbar-default">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#home">Tjebbe zoekt een job</a>
        </div>
        <div class="navbar-collapse collapse pull-right">
          <ul class="nav navbar-nav">
            <li class="active"><a href="#home">Home</a></li>
            <li><a href="#about">About</a></li>
            <li><a href="#studies">Studies</a></li>
            <li><a href="#school-projecten">School projecten</a></li>
            <li><a href="#projecten">Projecten</a></li>
            <li><a href="#contact">Contact</a></li>
            <li><a href="#cv">Mijn CV</a></li>
          </ul>
        </div>
      </div>
    </div>
    
    <div id="about" class="scrollspy">
      <div class="container">
        <h2>About me <small>Tjebbe Lievens</small></h2>
        <div class="row">
          <p>Ik ben Tjebbe Lievens en studeer een bachelor Elektronica - ICT optie ICT aan de Thomas More hogeschool in Geel. Aangezien ik momenteel in mijn laatste maanden/weken zit van mijn opleiding moet ik opnieuw een keuzen maken, verder studeren en een master proberen of werken. Kiezen is steeds verliezen, ofwel kies ik voor de opleiding en dus waarschijnlijk meer kennis na die studie. Ofwel kies is voor de ervaring die ik zal hebben na die zelfde periode. Een moeilijke keuze dus maar wel een belangrijke. Omdat ik niet weet wanneer mijn keuze gemaakt zal zijn wil ik toch niet blijven wachten tot dan en start ik nu al moet mijn zoektocht naar een job als ik dan niet ga verder studeren heb ik misschien toch al iets. Of als ik niet de job vind die mij ligt kan ik nog steeds kiezen voor het studeren.</p>
          <p>Nu al iets meer dan 2O jaar geleden op 27 december 1993 werd ik geboren in Geel waar ik nog steeds woon. Ik heb altijd al dingen willen doen die niet altijd even alledaags waren. Op mijn 11ste begon ik met wielrennen na al een voetbal carriere achter de rug te hebben. Jeugd wielrennen was toen nog niet zo populair als dat het nu is. In mijn middelbare school volgde ik eerst de richting Elektronica met als doel verder te gaan met iets ICT gerelateerd. Maar na het eerste jaar waren er enkele algemene vakken (Geschiedenis, ...) die me tegen hielden en ik iets anders moest doen en ik koos dan voor Elektro technieken. Ik studeerde dan ook in het 6de jaar af als elektro technieker TSO en kon beginnen bij bv. een zelfstandige om huis installaties te doen of in een industrieel bedrijf als onderhoudstechnieker. Ik koos voor iets heel anders ICT. Een 3 jarige bachelor waar ik iets vond wat me zeer hard interesseerde en wat me ook nog eens goed lag. Buiten de algemene vakken zoals wiskunde en PC technologie (Geschiedenis van de Computer en technologie) lukte alles redelijk tot zeer goed. Nu ik ook aan het einde van dit hoofdstuk ben ben ik dus opzoek naar iets nieuws.</p>
          <p></p>
        </div>
        <div class="row">
          <div class="col-md-6">
              <h3 class="with-border">My Skills</h3>
              <div class="progress-block">
                <div class="progress">
                  <div class="bar" style="width: 90%;"></div>
                  <span class="progress-name">Web developing</span>
                  <span class="value">90%</span>
                </div>
                <div class="progress">
                  <div class="bar " style="width: 75%;"></div>
                  <span class="progress-name">Software developing</span>
                  <span class="value">75%</span>
                </div>
                <div class="progress">
                  <div class="bar" style="width: 80%;"></div>
                  <span class="progress-name">Mobile developing</span>
                  <span class="value">80%</span>
                </div>  
                <div class="progress">
                  <div class="bar" style="width: 70%;"></div>
                  <span class="progress-name">Netwerk (CISCO)</span>
                  <span class="value">70%</span>
                </div>  
                <div class="progress">
                  <div class="bar" style="width: 80%;"></div>
                  <span class="progress-name">Servers</span>
                  <span class="value">75%</span>
                </div>  
              </div>
          </div>
          <div class="col-md-6">
            <div class="videoWrapper">
                <iframe src="//www.youtube.com/embed/CTH2YgaqTSY" frameborder="0" allowfullscreen></iframe>
              </div>
          </div>
        </div>
      </div>
    </div>

    <div id="apple" class="parallax">
      <div class="black-bg">
        <div class="text">
          <blockquote>
            <h2>Design is powerful</h2>
            "Design is not just what it looks like and feels like. Design is how it works"
            <small class="color">Steve Jobs</small>
          </blockquote>
        </div>
      </div>
    </div>

    <div id="studies" class="scrollspy">
      <div class="container">
        <h2>Studies</h2>
        <div class="row">
          <div class="md-col-4">

          </div>
          <div class="md-col-4">

          </div>
          <div class="md-col-4">

          </div>
        </div>
        <div class="row">
          <p>In het middelbaar start ik in het technisch onderwijs in de eerste graad was dat nog heel algemeen en leerde ik in alles wat hout bewerking, metaal bewerking, bouw, elektriciteit, technisch tekenen, ... In de 2de graad begon ik met Elektronica met als doel om in de 3de graad ICT te studeren. Maar na het 1ste jaar elektronica kwam ik net iets te kort in algemene vakken zoals geschiedenis, ... de technische gerichte vakken gingen wel heel goed toch moest ik een andere richting kiezen. Ik heb gekozen voor elektrotechnieken en behaalde daar ook mijn diploma in.</p>
          <p>Ik ging verder studeren en die keuze was snel gemaakt ICT toch twijfelde ik nog tussen toegepaste informatica maar Elektronica - ICT leek mij een breder les vulling te bieden. Het eerste jaar verliep zeer goed ik haalde tijdens de eerste examens een 20/20 op C# uiteindelijk haalde ik in januari een 19/20 op dat vak. Natuurlijk had ik ook mijn mindere vakken zoals wiskunde, PC Technologie, Licht en geluid. In het 2de kregen we nog meer programmeer vakken en toen wist ik dat dat iets voor mij was ook hier haalde ik goede punten. In het 3de jaar kwam ik ook niet al te veel moeilijke vakken tegen.</p>
        </div>
        <div class="row">
          <div class="col-md-6">
              <h3 class="with-border">Punten</h3>
              <label for="score-detail"><input type="checkbox" id="score-detail"> Meer details</label><br>
              <div class="progress-block">
                <div class="progress">
                  <div class="bar " style="width: 93%;"></div>
                  <span class="progress-name">Webdesign <span class="score-disable">(Gem.:basis 18 + gevorderden 20 + expert 18 op 20)</span></span>
                  <span class="value">93,33%</span>
                </div>
                <div class="progress">
                  <div class="bar" style="width: 90%;"></div>
                  <span class="progress-name">Java <span class="score-disable">(gem.: basis 19 + gevorderden 17 op 20)</span></span>
                  <span class="value">90%</span>
                </div>  
                <div class="progress">
                  <div class="bar" style="width: 85%;"></div>
                  <span class="progress-name">C# <span class="score-disable">(gem.: basis 19 + gevorderden 15 op 20)</span></span>
                  <span class="value">85%</span>
                </div>
                <div class="progress">
                  <div class="bar" style="width: 73%;"></div>
                  <span class="progress-name">Linux <span class="score-disable">(gem. basis 13 + gevorderden 17 + expert 14 op 20)</span></span>
                  <span class="value">73,33%</span>
                </div>
                <div class="progress">
                  <div class="bar" style="width: 67.5%;">Windows <span class="score-disable">(gem. Client 15 + basis 14 + gevorderden 12 + Exchange 13 op 20)</span></div>
                  <span class="progress-name"></span>
                  <span class="value">67,5%</span>
                </div>
                <div class="progress">
                  <div class="bar" style="width: 77%;">Netwerken <span class="score-disable">(gem. basis 14 + beveiliging 13 + routing 10 + switching 12 + Voip 13 + WAN 15 op 20)</span></div>
                  <span class="progress-name"></span>
                  <span class="value">77%</span>
                </div>
                <div class="progress">
                  <div class="bar" style="width: 90%;"></div>
                  <span class="progress-name">Mobile computing: Android<span class="score-disable">(18 op 20)</span></span>
                  <span class="value">90%</span>
                </div> 
                <div class="progress">
                  <div class="bar" style="width: 60%;"></div>
                  <span class="progress-name">Talen <span class="score-disable">(gem. frans 11 + engels 13 op 20)</span></span>
                  <span class="value">60%</span>
                </div> 
                <div class="progress">
                  <div class="bar" style="width: 80%;"></div>
                  <span class="progress-name">Database <span class="score-disable">(gem. basis 16 + gevorderden 16 op 20)</span></span>
                  <span class="value">80%</span>
                </div> 
                <div class="progress">
                  <div class="bar" style="width: 70%;"></div>
                  <span class="progress-name">Virtualisatie (VMWare ESXI, VCenter, Openfiler) <span class="score-disable">(14 op 20)</span></span>
                  <span class="value">70%</span>
                </div> 
              </div>
          </div>
          <div class="col-md-6">
            <div class="center">
              <a href="/images/punten.png" rel="nofollow" class="btn btn-lg btn-warning" target="_blank">Volledig overzicht</a>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div id="disney" class="parallax">
      <div class="black-bg">
        <div class="text">
          <blockquote>
            <h2>You can</h2>
            "If you can dream it, you can do it."
            <small class="color">Walt Disney</small>
          </blockquote>
        </div>
      </div>  
    </div>

    <div id="cloud" class="scrollspy">
      <div class="container">
        <div class="row"id="wordcloud" style="height: 350px;">

          </div>
        </div>
      </div>
    </div>

    <div id="da-vinci" class="parallax">
      <div class="black-bg">
        <div class="text">
          <blockquote>
            <h2>Simplicity</h2>
            "Simplicity is the ultimate sophistication."
            <small class="color">Leonardo da Vinci</small>
          </blockquote>
        </div>
      </div>  
    </div>

    <div id="school-projecten" class="scrollspy">
      <div class="container">
        <h2>School projecten</h2>
        <div class="row">
          <p>Op school hebben we ook heel wat projecten en/of opdrachten moeten maken. Hier vind u een klein overzicht.</p>
        </div>
        <div class="row">
          <div class="col-md-8">
            <h2>Grafisch design</h2>
            <p>In het eerste semester van mijn opleiding hebben we leren werken met fireworks, deze lessen waren niet zeer uitgebreid maar door zelf te oefenen kan ik er toch een eindje mee overweg.</p>
          </div>
          <div class="col-md-4">
            <img src="/images/portfolio_school_tumb.png" alt="">
          </div>
        </div>
        <hr>
        <div class="row">
          <div class="col-md-8 col-md-push-4">
            <h2>Mobile computing</h2>
            <p>Tijdens de lessen mobile computing waar we leerden werken met android heb ik ook een zeer groot project afgewerkt als opdracht. We mochten zelf kiezen wat we deden maar er waren wel heel wat vereisten. Ik ontwikkelde een nieuws applicatie gekoppeld aan een backend. De backend was geen onderdeel van de opdracht maar deze heb ik wel zelf ontwikkeld in PHP om mijn app te laten werken. Op de backend ontwierp ik een php crawler/spider die op 100den nieuws websites nieuws berichten ging zoeken. Deze werden opgeslagen in een mysql database en hier werden nog enkele zaken uit gegenereerd zoals sleutelwoorden, ... Via de android applicatie kon je makkelijk en snel nieuws berichten ophalen gesorteerd op land en categorie. Je kon het nieuws rechtstreeks in de app lezen of door surfen naar de website. De nieuws berichten waren offline ook leesbaar dus je moest geen internet verbinding hebben als je nieuws in de app wou lezen. Je kon zelf bepalen wanneer de artikelen gedownload werden, dit gebeurde automatisch na een in te stellen tijd. Er waren plannen om de app verder uit te breiden maar na enkele maanden was de database vol gelopen met enkele miljoenen nieuwsberichten waardoor het onmogelijk werd om het systeem op een zo goedkoop mogelijke manier te laten werken.</p>
          </div>
          <div class="col-md-4 col-md-pull-8">
            <img src="/images/portfolio_news_reader_tumb.png" alt="">
          </div>
        </div>
        <hr>
        <div class="row">
          <div class="col-md-8">
            <h2>C#</h2>
            <p>Tijdens de lessen C# gevorderden en sturingen moesten we een project ontwikkelen. Tijdens C# gevorderden ontwierp ik een sport scorebord waarmee heel wat informatie kon worden uitgelezen en opgeslagen in een database. De toepassing bevatte een standaard scorebord met timer functie en een functie om bij te houden welke speler op welk moment een punt scoorde. Met de database connectie kon dit allemaal worden opgeslagen en werden zo automatisch klassementen gegenereerd. Door tijdens de wedstrijd enkele gegevens bij te houden konden er dan ook statistieken per speler, team of wedstrijd opgevraagd worden.</p>
            <p>Tijdens C# sturingen moesten we een groepswerk maken en enkele dingen aansturen d.m.v. bleutooth, seriële verbindingen of TCP/IP. We kozen voor een rollercoaster met een centraal beheersysteem dat alles controleerde en informatie doorstuurde naar de andere controllers. Aan de instap locatie kon via een display de status worden uitgelezen als ook de attractie worden gestart. Na het starten werden er enkele motoren aangestuurd waar dat het toerental gecontroleerd werd en waar nodig bijgestuurd. De remmen die in de afdaling het karretje moesten doen afremmen waren zo opgebouwd dat bij een systeemcrash niets fout kan lopen. Via het controle paneel aan de start locatie kon je alles volgen, bij eventueel defect kon men daar ook de problemen uitlezen.</p>
          </div>
          <div class="col-md-4">
            <img src="" alt="">
          </div>
        </div>
        <hr>
        <div class="row">
          <div class="col-md-8 col-md-push-4">
            <h2>Project werk1</h2>
            <p>Enkele uren per week in het 2de semester van het eerste jaar werkten we in groepsverband aan een opdracht. Het was de bedoeling om video bewerkings software en camera technieken te leren a.d.h.v. een opdracht het maken van een kort film. Hierdoor leerden we werken met enkele software toepassingen zoals adobe after effects en adobe premiere. Zo konden we special effects gebruiken in onze film zoals green screen, ...</p>
          </div>
          <div class="col-md-4 col-md-pull-8">
            <img src="" alt="">
          </div>
        </div>
        <hr>
        <div class="row">
          <div class="col-md-8">
            <h2>Projectwerk 2</h2>
            <p>Tijdens het 2de jaar werkten we eerst in kleine groepjes een heel plan uit voor een bedrijfs omgeving, we moesten met alles rekeninghouder, hardware, software en netwerk. We moesten een volledige planning maken met al de gegevens die we nodig hebben om een heel lokaal bedrijfs klaar te maken in 1 week tijd. Dit werd gekoppeld aan een wedstrijd na het voorstellen van onze projecten aan de jury werd het beste project gekozen dat zou worden uitgewerkt met heel de klas. Mijn team won niet maar we mochten ons bezig houden met de website en het backup systeem. Ik heb mij vooral toegelegd op de website. De website was bedoeld voor een computer winkel met bijhorende webshop. Omdat het project redelijk uitgebreid was kozen we er voor om de website volledig op te bouwen met de toen nog beperkte kennis van de web programmeer talen. Toch konden we onze opdracht tot een zeer goed einde brengen.</p>
          </div>
          <div class="col-md-4">
            <img src="" alt="">
          </div>
        </div>
        <hr>
        <div class="row">
          <div class="col-md-8 col-md-push-4">
            <h2>Embedded server</h2>
            <p>In de richting ICT kregen we ook een beetje elektronica een deel daarvan was werken met een microcontroller en ook hier moesten we een opdracht op verzinnen. Met enkele mede collega's hebben we via een microcontroller een domotica systeem ontwikkeld dat via een web interface benaderd kon worden.</p>
          </div>
          <div class="col-md-4 col-md-pull-8">
            <img src="" alt="">
          </div>
        </div>
        <hr>
        <div class="row">
          <div class="col-md-8">
            <h2>Webdesign</h2>
            <p>Tijdens de verschillende lessen over webdesign hebben we ook een heel aantal oefeningen moeten maken in het begin moesten we websites na bouwen maar op het einde van het lessen pakket waren we instaat om een deftige website te ontwikkelen in HTML, CSS, JavaScript (JQuery) en PHP.</p>
          </div>
          <div class="col-md-4">
            <img src="" alt="">
          </div>
        </div>
        <hr>
        <div class="row">
          <div class="col-md-8 col-md-push-4">
            <h2>Java gevorderen</h2>
            <p>Tijdens de laatste java lessen moesten we een game ontwikkelen in java. Door dat ik java toch wel redelijk goed onder de knie heb wou ik iets tof maar mijn ideeën waren zeer groot misschien wel iets te groot. Ik heb het spel zo ver mogelijk proberen ontwikkelen maar door tijdsgebrek ben ik niet geraakt waar ik wou geraken. Daardoor ben ik zelf teleurgesteld in mijn werk maar het blijft nog steeds een goed werk mits ik er nog wat tijd insteek. Al zal de verder ontwikkeling niet vanzelf lopen de code bestaat uit +5000 lijnen en om de code terug te begrijpen zal ik dus enkele uren tijd moeten vrij maken.</p>
          </div>
          <div class="col-md-4 col-md-pull-8">
            <img src="/images/portfolio_fruit_world_tumb.png" alt="">
          </div>
        </div>
        <hr>
        <div class="row">
          <div class="col-md-8">
            <h2>Projectwerk3</h2>
            <p>Om ons laatste jaar af te sluiten moesten we met nog 4 andere leerlingen een maand lang aan een project werken, we mochten zelf voorstellen doen maar enkele docenten hadden toffe opdrachten die ons ook wel tof leken. Omdat we nu tijdens de lessen webdesign php leren programmeren via dreamweaver wat niet echt veilig is moesten we onderzoek doen naar het framework laravel. Dit om te kijken hoe moeilijke of makkelijk het is om een website op te bouwen. Om onze layout wat makkelijker te maken gebruikten we bootstrap. Om laravel te testen ontwikkelde we op deze 4 weken 2 websites. 1website werd een kook website met meer dan 400 recepten. De 2de website werd een website voor de studentenraad van Thomas More.</p>
          </div>
          <div class="col-md-4">
            <img src="/images/portfolio_studentenraad_khk_be_tumb.png" alt="">
          </div>
        </div>
        <hr>
        <div class="row">
          <div class="col-md-8 col-md-push-4">
            <h2>Stage</h2>
            <p>Om mijn laatste jaar volledig af te ronden moet ik 3maanden een stage lopen, we mochten zelf kiezen welk bedrijf. Ik doe mijn stage bij Crius Group in Hulshout, in dit tof bedrijf kreeg ik een zeer uitdagende opdracht waar ik veel door bijleer. Ik leer heel wat nieuwe en oudere technieken kennen die ik kan gebruiken tijdens mijn weg naar de perfecte oplossing.</p>
          </div>
          <div class="col-md-4 col-md-pull-8">
            <img src="" alt="">
          </div>
        </div>
        <hr>
        <div class="row">
          <div class="col-md-4 col-md-offset-4">
            <center>
            <h2>Wil je meer info?</h2>
            <a href="#contact" class="btn btn-primary">Vraag het hier!</a>
          </center>
          </div>
        </div>
        <div class="row" style="margin-bottom: 50px;">
          <div class="col-md-12">
            <h4>Van de meeste projecten ben ik het bezit van de source code of werkende toepassingen, in veel gevallen is het mogelijk om deze te verkrijgen.</h4>
          </div>
        </div>
      </div>
    </div>

    <div id="cook" class="parallax">
      <div class="black-bg">
        <div class="text">
          <blockquote>
            <h2>Have a fun</h2>
            "Creativity is inventing, experimenting, growing, taking risks, breaking rules, making mistakes, and having fun"
            <small class="color">Mary Lou Cook</small>
          </blockquote>
        </div>
      </div>  
    </div>

    <div id="projecten" class="scrollspy">
      <div class="container">
        <h2>Eigen projecten</h2>
        <p>Ik heb tijdens mijn studentenleven zelf ook niet stil gezeten. Al doende leer je de technieken pas echt. Daarom heb ik heel wat websites en toepassingen ontwikkeld of zijn nog in ontwikkeling.</p>
        <p>Op mijn 12de begon ik al met het maken van websites in HTML en PHP. Later leerde ik zelf ook nog VB.NET, onderstaande projecten zijn maar enkele van mijn laatst afgewerkte (of belangrijkste).</p>
        <div class="row">
          <div class="row">
            <div class="col-md-8 col-md-push-4">
              <h2>Android game framework</h2>
              <p>Tijdens de lessen Java leerde ik al wat game developing in Java. Simpele 2D games hebben dan ook nog maar weinig geheimen voor mij. Maar games op PC's zijn niet meer hip en trendy, ik wou weten hoe dit werkte op smartphones. Android is gebaseerd op Java dus was het omvormen van code niet zo heel moeilijk. Daarom ontwikkelde ik een framework zodat ik mijn Java games snel zou kunnen omvormen naar android met natuurlijk de nodige exta's zoals Facebook en Twitter integratie. Het is een zeer goed framework en ik heb ook even getwijfeld om dit om te vormen naar IOS maar er bestaan al heel wat frameworks, op IOS maak ik dan ook gebruik van Cocos 2D.</p>
            </div>
            <div class="col-md-4 col-md-pull-8">
              <img src="/images/portfolio_app_flappy_bird_tumb.png" alt="">
            </div>
          </div>
          <hr>
          <div class="row">
            <div class="col-md-8">
              <h2>aardbeien-asperges.be</h2>
              <p>Mijn ouders hebben een eigen bedrijf in het telen van aardbeien en asperges. Mijn ouders zijn helemaal niet technisch bezig met internet en pc, toch heb ik hen kunnen overhalen om een website op te zetten. De website wordt opgezet in verschillende fases, in de eerste fase zal de website vooral gericht zijn op het informeren over deze 2 lekkere producten. In de volgende fases gaan we de website meer rond het bedrijf bouwen waar dat uiteindelijk het bedrijf meer centraal komt te staan op de website. Momenteel is de website in fase 1 maar de bouwen van fase 2 is al een tijdje aan de gang. In fase 2 zal de website groten deels het zelfde blijven hier en daar enkele verbeteringen, maar niet te veel aanpassingen zodat de klant of bezoeker geen al te grote overstap moeten maken en alles vlotjes kan blijven vinden. In fase 2 zal er ook meer functionaliteit zijn voor de huidige klant, zo kunnen grotere afnemers hun facturen en aankopen beheren via een handig admin paneel. Ook zal er een betere integratie zijn met onze social media kanalen om zo de klanten beter te kunnen bereiken.</p>
            </div>
            <div class="col-md-4">
              <img src="/images/portfolio_aardbeien-asperges_be_tumb.png" alt="">
            </div>
          </div>
          <hr>
          <div class="row">
            <div class="col-md-8 col-md-push-4">
              <h2>Cyclingnieuws.be</h2>
              <p>Cyclingnieuws is een website die ik heb opgebouwd in de zomer van 2012. Ik wou de website rustig laten uitgroeien en was vooral gericht op het analyseren van wedstrijden en uitslagen zodat renners een beter overzicht zouden krijgen in hun gereden wedstrijden. Door een plotseling groot succes werd het moeilijk om de website te beheren, de database was al redelijk gevult en door de opkomst van het succes wouden we meer informatie verzamelen. Door al de druk die ik me zelf en het hele systeem oplegde faalde het dan ook. Het systeem werd moeilijk beheerbaar omdat we er zoveel mogelijk informatie in wouden en daarna uitlaten komen. Door falende servers en gebrek aan ervaring heb ik dan ook besloten om de website offline te halen en volledig opnieuw op te bouwen zodat ze goed in elkaar zit. Momenteel ben ik nog steeds bezig aan de opbouw hiervan, door het verleden heb ik heel wat geleerd en zal hopenlijk opgewassen zijn tegen een mogenlijk opnieuw succes.</p>
            </div>
            <div class="col-md-4 col-md-pull-8">
              <img src="/images/portfolio_cyclingnieuws_be_2_tumb.png" alt="">
            </div>
          </div>
          <hr>
          <div class="row">
            <div class="col-md-8">
              <h2>belwaarde.net</h2>
              <p>Als je een prepaid betaalkaart hebt in je smartphone en je vaak binnen je limieten blijft kan het wel eens gebeuren dat je belkrediet hoog oploopt. Wist je trouwens dat die belwaarde na een half jaar (afhankelijk van welke provider) vervalt? Om dit probleem te voorkomen heb ik een simpele oplossing bedacht, een methode om je overtollig belkrediet terug om te zetten in geld op je bankrekening of paypal. In eerste instantie ontwikkelde ik het systeem voor mezelf maar na een tijdje besloot ik toch om het openbaar voor iedereen te maken. Het systeem werkt eigenlijk vrij eenvoudig, je krijgt een unieke code op onze website en een telefoon nr, tijdens het bellen naar dit telefoon nr wordt de unieke code gevraagd en deze geef je door. Na een succes vol gesprek van enkele minuten is er een op voor hand bepaald bedrag van je simkaart verdwenen, je krijgt dan iets meer dan de helft van dat bedrag op terug gestort. Ook heb ik de mogelijkheid ingebouwd om a.d.h.v. enkele parameters een berekening te maken welk abonnement het best bij jouw past. Ook is er een android app ontwikkeld die nu getest wordt met de zelfde functionaliteit en ook een verbruiksmeter zo kunnen we a.d.h.v. je verbruik meteen bepalen of je een goede keuze hebt gemaakt bij het aanschaffen van je abonnement. Ook werken we aan een IOS en Windows Phone app.</p>
            </div>
            <div class="col-md-4">
              <img src="/images/portfolio_belwaarde_net_2_tumb.png" alt="">
            </div>
          </div>
          <hr>
          <div class="row">
            <div class="col-md-8 col-md-push-4">
              <h2>Studentencodex Android App (<a href="http://studentencodex.tpweb.org" target="_blank">Lees meer</a>)</h2>
              <p>In februari 2014 kreeg ik via via te horen dat een bedrijf opzoek was naar enkele android developers om hun bekende iphone app te ontwikkelen voor android. Een uitdaging die ik meteen aanging samen met een collega ontwikkelaar. Na enkele weken rustig developen was de app klaar en ze is nu dan ook live. Na 36uur dat de app in de play store stond hadden we al meer dan 100 downloads, dagelijks komen hier nog enkele downloads bij.</p>
            </div>
            <div class="col-md-4 col-md-pull-8">
              <img src="http://studentencodex.tpweb.org/img/slide-1.jpg" alt="">
            </div>
          </div>
          <hr>
          <div class="row">
            <div class="col-md-8">
              <h2>Social media managment (PokeTik.com)</h2>
              <p>Social media websites zijn de laatste jaren niet meer uit ons leven weg te denken, maar het zijn er ondertussen al heel wat geworden. Voor bedrijven en mensen dus nog bijna onmogelijk om deze allemaal te volgen, enkele jaren geleden begon ik aan de ontwikkeling van mijn eigen social netwerk maar door de komst van veel andere social netwerk sites heb ik mijn ontwikkeling gepauzeerd omdat zulke websites niet op te zetten zijn zonder een degelijk budget. Ik ontwikkelde een ander systeem gebaseerd op het systeem om meerdere social netwerk sites te beheren van op 1 plaats. Momenteel werk ik nog steeds aan dit project en wordt het al uitvoerig getest door enkele social media kanalen die ik onderhoud.</p>
            </div>
            <div class="col-md-4">
              <img src="" alt="">
            </div>
          </div>
          <hr>
          <div class="row">
            <div class="col-md-8 col-md-push-4">
              <h2>Babble app</h2>
              <p>Door de opkomst van veel social media websites en chat applicaties is het steeds moeilijker geworden om in contact te blijven met al je vrienden en familie. De ene contacteer je via SMS de andere via facebook messanger of via whatsapp, met sommige contacten communiceer je wel eens via verschillende kanalen. Na verloop van tijd heb je 10tallen apps om te communiceren met je vrienden en raak je er bijna in verdwaald. Ik bedacht hier en oplossing voor, een app die alles centraal beheerd. Chat via facebook met je vrienden zijn de offline kan je makkelijk verder sms en dat binnen één zelfde applicatie en pagina. Heb je zin om even een foto te versturen of om even te bellen geen probleem, gebruik je liever skype? Dan sturen we je meteen door naar de juiste app. Alles van op 1 plaats overal bruikbaar. We bieden je zelfs de mogelijkheid om je sms overal mee te nemen, via je webrowser even je sms berichten lezen en meteen ook beantwoorden zonder je smartphone te moeten bovenhalen. Heeft je smartphone toch geen verbinding met onze servers dan is het mogelijk om een sms te versturen via onze servers.<br>
                Deze app is natuurlijk niet in 1 2 3 ontwikkeld en is dus nog in ontwikkeling en test fase.</p>
            </div>
            <div class="col-md-4 col-md-pull-8">
              <img src="/images/portfolio_babble_tumb.png" alt="">
            </div>
          </div>
          <hr>
          <div class="row">
            <div class="col-md-8">
              <h2>Save login</h2>
              <p>Inloggen via een wachtwoord bestaat al heel lang, dagelijks lees je wel ergens dat er weer eens duizenden of miljoenen wachtwoorden verzameld werden. Inloggen zonder wachtwoord lijkt misschien nog verre toekomst maar daar denk ik toch anders over. Ik ben nl. bezig met het ontwikkelen van heel wat verschillende inlog methodes via bv. via je smartphone. Ik heb zelfs enkele methodes waar het mogelijk is om zonder enige ingaven van je gegevens op jouw persoonlijke beveiligde pagina te komen.</p>
            </div>
            <div class="col-md-4">
              <img src="" alt="">
            </div>
          </div>
          <hr>
          <div class="row portfolio-images">
            <div class="col-md-4 col-sm-6">
              <a href="http://www.aardbeien-asperges.be" target="_blank">
                <img src="/images/portfolio_aardbeien-asperges_be_tumb.png" alt="">
              </a>
            </div>
            <div class="col-md-4 col-sm-6">
              <a href="http://www.belwaarde.net" target="_blank">
                <img src="/images/portfolio_belwaarde_net_tumb.png" alt="">
              </a>
            </div>
            <div class="col-md-4 col-sm-6">
              <a href="" data-toggle="modal" data-target="#WkFBCover2">
                <img src="/images/portfolio_fb_cover_WK_PG_2_tumb.png" alt="">
              </a>
            </div>
            <div class="col-md-4 col-sm-6">
              <a href="" data-toggle="modal" data-target="#WkFBCover2">
                <img src="/images/portfolio_fb_cover_WK_PG_tumb.png" alt="">
              </a>
            </div>
            <div class="col-md-4 col-sm-6">
              <a href="" data-toggle="modal" data-target="#dirklievens">
                <img src="/images/portfolio_flyer_dirk_lievens_tumb.png" alt="">
              </a>
            </div>
            <div class="col-md-4 col-sm-6">
              <a href="" data-toggle="modal" data-target="#flyer">
                <img src="/images/portfolio_flyer_lg_tumb.png" alt="">
              </a>
            </div>
            <div class="col-md-4 col-sm-6">
              <a href="" data-toggle="modal" data-target="#fonteintjes">
               <img src="/images/portfolio_geel_fonteintjes_tumb.png" alt="">
             </a>
            </div>
            <div class="col-md-4 col-sm-6">
              <a href="https://www.youtube.com/watch?v=rTPQp2HcGdI" target="_blank">
               <img src="/images/portfolio_lyrics_flo_rida_tumb.png" alt="">
             </a>
            </div>
            <div class="col-md-4 col-sm-6">
              <a href="https://www.youtube.com/watch?v=I6heugQ5Wc0" target="_blank">
                <img src="/images/portfolio_lyrics_follow_tumb.png" alt="">
              </a>
            </div>
            <div class="col-md-4 col-sm-6">
              <a href="https://www.youtube.com/watch?v=yNAYjXOWMrw" target="_blank">
                <img src="/images/portfolio_lyrics_Labrinth_tumb.png" alt="">
              </a>
            </div>
            <div class="col-md-4 col-sm-6">
              <a href="" data-toggle="modal" data-target="#picedit">
                <img src="/images/portfolio_pic_edit_tumb.png" alt="">
              </a>
            </div>
            <div class="col-md-4 col-sm-6">
              <a href="" data-toggle="modal" data-target="#school">
                <img src="/images/portfolio_school_tumb.png" alt="">
              </a>
            </div>
            <div class="col-md-4 col-sm-6">
              <a href="http://www.siebelievens.be" target="_blank">
                <img src="/images/portfolio_siebelievens_be_tumb.png" alt="">
              </a>
            </div>
            <div class="col-md-4 col-sm-6">
              <a href="http://www.tpweb.org" target="_blank">
                <img src="/images/portfolio_tpweb_org_tumb.png" alt="">
              </a>
            </div>
            <div class="col-md-4 col-sm-6">
              <a href="http://www.cyclingnieuws.be" target="_blank">
                <img src="/images/portfolio_cyclingnieuws_be_tumb.png" alt="">
              </a>
            </div>
            <div class="col-md-4 col-sm-6">
              <a href="" data-toggle="modal" data-target="#flappy">
                <img src="/images/portfolio_app_flappy_bird_tumb.png" alt="">
              </a>
            </div>
            <div class="col-md-4 col-sm-6">
              <img src="" alt="">
            </div>
          </div>
        </div>
      </div>
    </div>

    <div id="rostand" class="parallax">
      <div class="black-bg">
        <div class="text">
          <blockquote>
            <h2>Think</h2>
            "Think? Why think! We have computers to do that for us."
            <small class="color">Jean Rostand</small>
          </blockquote>
        </div>
      </div>  
    </div>

    <div id="contact" class="scrollspy">
      <div class="container" style="margin-bottom: 50px;">
        <h2><i class="icon-map-marker"></i> Get in touch</h2>
        <div class="row">
          <div class="col-md-8">
            <h3>Want to ask something?</h3>
            <form role="form" action="/contact" method="post">
              <div class="alert alert-success" id="send" style="display: none;">Ik neem zo snel mogelijk contact met u op!</div>
              <div class="form-group">
                <input type="text" placeholder="Name" name="name" class="form-control">
              </div>
              <div class="form-group">
                <input type="email" class="form-control" placeholder="Email" name="email">
              </div>
              <div class="form-group">
                <input type="text" class="form-control" placeholder="Subject" name="subject">
              </div>
              <div class="form-group">
                <textarea class="form-control" placeholder="Message" name="message" rows="6"></textarea>
              </div>
              <input type="submit" class="btn btn-large pull-right" value="Submit">
            </form>
          </div>
          <div class="col-md-4">
              <h3>My information</h3>
              <div class="row">
                <div class="col-sx-12">
                  <address>
                    Tjebbe Lievens<br>
                    Schransdijk 7A<br>
                    2440 Geel<br/>
                    Belgium
                  </address>
                </div>
                <div class="col-sx-12">
                  <p><i class="icon-phone"></i> +32 (0)475 37 55 57</p>
                </div>
                <div class="col-sx-12"> 
                  <p><i class="icon-envelope"></i> <a href="mailto:tjebbelievens@hotmail.com">tjebbelievens@hotmail.com</a></p>
                </div>  
              </div>  
              <div class="col-sx-12 footer-icons">
                <a href="http://www.twitter.com/tjebbelievens" title="" target="_blank">
                  <i class="icon-twitter"></i>
                </a>
                <a href="http://www.facebook.com/tjebbe.lievens" title="" target="_blank">
                  <i class="icon-facebook"></i></a>
                <a href="http://www.google.com/+tjebbelievens" title="" target="_blank">
                  <i class="icon-google-plus"></i></a>
                <a href="https://www.linkedin.com/in/tjebbelievens" title="" target="_blank">
                  <i class="icon-linkedin"></i></a>
                <a href="https://github.com/tjebbeke" title="" target="_blank">
                  <i class="icon-github"></i></a>
                <a href="https://github.com/TPWeb" title="" target="_blank">
                  <i class="icon-github"></i></a>
                <a href="https://bitbucket.org/tpweb" title="" target="_blank">
                  <i class="icon-bitbucket"></i></a>
              </div>
              <div class="col-sx-12 footer-icons">
               U kan mij het best bereiken via mail.
              </div>
          </div>
        </div>
      </div>
    </div>

    <div id="cv" class="scrollspy dark-bg">
      <div class="container text-center">
        <h3 class="title">Mijn CV</h3>
        <p class="subtitle">Bekijk hier mijn CV.</p>

        <form action="/cv" method="POST" class="form-subscription" role="form">
          <div class="input-group">
            <input type="email" name="email" class="form-control input-lg" placeholder="Enter your email">
            <span class="input-group-btn">
              <button class="btn btn-danger btn-lg" type="submit">Download!</button>
            </span>
          </div>
        </form>
        <p>U bent niet verplicht om je e-mail op te geven! Het zou wel fijn zijn!</p>

        <ul class="list-inline social">
          <li><a href="http://www.facebook.com/tjebbe.lievens" target="_blank"><i class="fa fa-facebook"></i></a></li>
          <li><a href="http://www.twitter.com/tjebbelievens" target="_blank"><i class="fa fa-twitter"></i></a></li>
          <li><a href="https://github.com/tjebbeke" target="_blank"><i class="fa fa-github"></i></a></li>
          <li><a href="https://bitbucket.org/tpweb" target="_blank"><i class="fa fa-bitbucket"></i></a></li>
          <li><a href="skype:tjebbelievens@hotmail.com?chat" target="_blank"><i class="fa fa-skype"></i></a></li>
          <li><a href="https://www.linkedin.com/in/tjebbelievens" target="_blank"><i class="fa fa-linkedin"></i></a></li>
          <li><a href="http://www.google.com/+tjebbelievens" target="_blank"><i class="fa fa-google-plus"></i></a></li>
        </ul>

      </div>
    </div>
    

    <footer>
      <div class="copyright-block clearfix">
        <div class="container">
          <div class="row text-center" style="margin-top: 30px; margin-bottom: 30px">
            <p>Copyright © 2014 by <a href="http://www.tpweb.org" target="_blank">Tjebbe Lievens</a>. All Rights Reserved</p>
          </div>
        </div>
      </div>  
    </footer>


<div class="modal fade" id="WkFBCover2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">WK FB Cover 2</h4>
      </div>
      <div class="modal-body">
        <img src="/images/portfolio_fb_cover_WK_PG_2_tumb.png">
        <p>Als wielrenner ben ik natuurlijk ook een supporter van onze belgische toppers. Met de overwinning van Gilbert tijdens het WK ontwierp ik enkele facebook covers voor een facebook pagina.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="WkFBCover" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">WK FB Cover</h4>
      </div>
      <div class="modal-body">
        <img src="/images/portfolio_fb_cover_WK_PG_tumb.png">
        <p>Als wielrenner ben ik natuurlijk ook een supporter van onze belgische toppers. Met de overwinning van Gilbert tijdens het WK ontwierp ik enkele facebook covers voor een facebook pagina.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="dirklievens" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Flyer</h4>
      </div>
      <div class="modal-body">
        <img src="/images/portfolio_flyer_dirk_lievens_tumb.png">
        <p>Tijdens de afgelopen gemeenteraads verkiezingen kwam mijn vader op als schepen van Mobiliteit, Openbare werken, Landbouw, ... Door mijn proef flyer, konden de ontwerpers sneller en makkelijker het uiteindelijke flyertje ontwerpen.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="fonteintjes" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Fonteintjes</h4>
      </div>
      <div class="modal-body">
        <img src="/images/portfolio_geel_fonteintjes_tumb.png">
        <p>Door de her aanleg van de markt werd er een fonteintjes voorzien om de markt wat op te vullen. Deze fonteintjes worden aangestuurd door wat elektronica zodat de kleur en hoogte geregeld kan worden. Ik heb dan ook de markt kunnen voorzien van de nodige licht en spektakel show door de fonteintjes te programmeren.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="flyer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Flyer</h4>
      </div>
      <div class="modal-body">
        <img src="/images/portfolio_flyer_lg_tumb.png">
        <p>Enkele jaren geleden was er een open bedrijven dag dat georganiseerd werd door een plaatselijke vereniging, ik ontwierp de affiches en flyers.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="picedit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Fotografie, foto bewerking</h4>
      </div>
      <div class="modal-body">
        <img src="/images/portfolio_pic_edit_tumb.png">
        <p>Ik ben af en toe ook wel eens graag bezig met fotografie en dus ook met foto bewerking.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="school" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">School</h4>
      </div>
      <div class="modal-body">
        <img src="/images/portfolio_school_tumb.png">
        <p>Werken met photoshop, after effects en andere toffe applicaties is ook één van mijn bezigheden.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="flappy" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Android game framework</h4>
      </div>
      <div class="modal-body">
        <img src="/images/portfolio_app_flappy_bird_tumb.png">
        <p>Het ontwikkelen van awesome games zoals flappy bird op een zelf geschreven framework.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>



    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="{{ URL::asset('js/jquery-1.10.2.min.js') }}"><\/script>')</script>
    <script src="{{ URL::asset('http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('js/main.js') }}"></script>
    <script>
    $(function() {
      $('#score-detail').change(function(event) {
        if($(this).is(':checked')) {
          $('span.score-disable').addClass('score-enable').removeClass('score-disable');
        } else {
          $('span.score-enable').addClass('score-disable').removeClass('score-enable');
        }
      });
      $('#contact form').submit(function(e) {
        e.preventDefault();
        $.post('/contact', $(this).serialize(), function(data) {
          $('#contact #send').fadeIn();
        });
      });


      $(document).scroll(function() {
        if($('#contact').offset().top - $(window).scrollTop() <= 0) {
          $('.navbar a').css('color', '#54ACF0');
        } else if($('#projecten').offset().top - $(window).scrollTop() <= 0) {
          $('.navbar a').css('color', '#f1c40f');
        } else if($('#school-projecten').offset().top - $(window).scrollTop() <= 0) {
          $('.navbar a').css('color', '#34495e');
        } else if($('#cloud').offset().top - $(window).scrollTop() <= 0) {
          $('.navbar a').css('color', '#e67e22');
        } else if($('#studies').offset().top - $(window).scrollTop() <= 0) {
          $('.navbar a').css('color', '#1abc9c');
        } else if($('#about').offset().top - $(window).scrollTop() <= 0) {
          $('.navbar a').css('color', '#e74c3c');
        }
      });

      var word_list = [
        {text: "Developer", weight: 30},
        {text: "Webdesign", weight: 20},
        {text: "PHP", weight: 15},
        {text: "jQuery", weight: 15},
        {text: "JavaScript", weight: 15},
        {text: "Java", weight: 13},
        {text: "C#", weight: 13},
        {text: "Smartphone applications", weight: 20},
        {text: "Android", weight: 15},
        {text: "IOS", weight: 15},
        {text: "Windows Phone", weight: 13},


        {text: "Sporten", weight: 10},
        {text: "Fietsen", weight: 10},
        {text: "Lopen", weight: 10},
        {text: "Reizen", weight: 10},
        {text: "Feesten", weight: 10},


        {text: "Doorzetter", weight: 12},
        {text: "Don't Dream Your Life, Live Your Dreams...", weight: 12},
        {text: "The sky isn't the limit", weight: 12},
        {text: "Rustig", weight: 12},
        {text: "Niet snel tevrede", weight: 12},
      ];
      $("#wordcloud").jQCloud(word_list);
    });
    </script>
  </body>
</html>