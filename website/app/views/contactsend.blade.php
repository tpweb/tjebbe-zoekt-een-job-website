<!DOCTYPE html>
<html lang="nl">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Contact, Studentencodex, de bijbel voor studenten is nu ook beschikbaar op android.">
    <meta name="keywords" content="Studentencodex, bijbel, studenten, android, app, iphone, contact">
    <meta name="author" content="Tjebbe Lievens">
    <meta name="csrf-token" content="<?= csrf_token() ?>">
    <!--<link rel="shortcut icon" href="{{ URL::asset('favicon.ico') }}"> -->
    <title>Studentencodex android app</title>
    {{ HTML::style('css/bootstrap.min.css') }}
    {{ HTML::style('css/font-awesome.min.css') }}
    {{ HTML::style('css/styles.css') }}
    <link id="scrollUpTheme" href="css/themes/image.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
    <script src="js/modernizr.custom.js"></script>
    <!-- Favicons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/ico/apple-touch-icon-144-precomposed.png">
    <link rel="shortcut icon" href="img/ico/favicon.png">

    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-48904183-5', 'tpweb.org');
  ga('send', 'pageview');

</script>
  </head>
  <body>
    <header id="header" class="header page">
      <nav class="navbar navbar-default navbar-static-top" role="navigation">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navabr-1">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">Studentencodex</a>
          </div>
          <div class="collapse navbar-collapse" id="navabr-1">
            <ul class="nav navbar-nav">
              <li class="active"><a href="/">Home</a></li>
              <li><a href="http://tjebbezoekteenjob.tpweb.org">Tjebbe zoekt een job</a></li>
              <li><a href="/about">About</a></li>
            </ul>
          </div>
        </div>
      </nav>

      <nav class="navbar navbar-default navbar-scroll navbar-fixed-top hidden" data-top-offset="500" role="navigation">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navabr-2">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">Studentencodex</a>
          </div>
        </div>
      </nav>
      <div class="container">
        <div class="header-description pull-left">
          <h1 class="title">Studentencodex: Contact</h1>
        </div>
        
        <div class="app-store-buttons pull-right">
          <a class="btn btn-danger" href="/android"><i class="fa fa-android"></i> Download free</a>
          <a class="btn btn-success" href="/apple"><i class="fa fa-apple"></i> Download free</a>
        </div> 
      </div>
    </header>
    
    <div class="page-content">
    
      <div class="container">
        <div class="row">
          <div class="col-sm-4">
          
            <p></p>
      
          </div>
          
          <div class="col-sm-8">
          
            <p class="alert alert-success">Contact formulier is verzonden!</p> 
            <div class="divider-2"></div>
            <p>&nbsp;</p> 
            <div class="divider-2"></div>
            <p>&nbsp;</p> 
            <div class="divider-2"></div>
            <p>&nbsp;</p> 
            <div class="divider-2"></div>
            <p>&nbsp;</p> 
            <div class="divider-2"></div>
            <p>&nbsp;</p> 
            <div class="divider-2"></div>
            <p>&nbsp;</p> 
            <div class="divider-2"></div>
            <p>&nbsp;</p> 
            <div class="divider-2"></div>
           
          </div>
        </div>
      </div>
    </div>
    
    <footer id="footer" class="footer">
      <div class="container">
        <div class="row">
          <div class="col-md-3">
            <a class="footer-logo" href="/">Studentencodex</a>
          </div>
          
          <div class="col-sm-4">
            <p class="footer-description">Deze app is gratis en werd ook gratis ontwikkeld in opdracht van Ice design.</p>
          </div>

          <div class="col-sm-2">
            <h4 class="title">Follow Tjebbe</h4> 
            <ul class="list-unstyled">
              <li><a href="https://www.facebook.com/tjebbe.lievens">Facebook</a></li>
              <li><a href="https://twitter.com/tjebbelievens">Twitter</a></li>
              <li><a href="https://github.com/tjebbeke">GitHub</a></li>
              <li><a href="https://github.com/tpweb">GitHub</a></li>
              <li><a href="https://www.linkedin.com/in/tjebbelievens">Linkedin</a></li>
              <li><a href="https://plus.google.com/+TjebbeLievens">Google +</a></li>
            </ul>
          </div>

          <div class="col-sm-2">
            <h4 class="title">Follow Ice Design & Studentencodex</h4> 
            <ul class="list-unstyled">
              <li><a href="https://www.facebook.com/icedesignbvba">Facebook (Ice Design)</a></li>
              <li><a href="https://www.facebook.com/pages/Studentencodex-iPhone-App/295237413313">Facebook (Studentencodex)</a></li>
              <li><a href="https://www.twitter.com/icedesign">Twitter</a></li>
              <li><a href="https://github.com/jdebeckker">GitHub</a></li>
              <li><a href="https://www.linkedin.com/company/ice-design-bvba">Linkedin</a></li>
            </ul>
          </div>

          <div class="col-sm-12">
            <p><a href="index.html#">Studentencodex</a> &copy; All Right Reserved</p>
          </div>

        </div>
      </div>
      
    </footer>

    <!-- Modal Button Danger -->
    <button class="btn btn-lg btn-hmcf btn-primary" data-toggle="modal" data-target="#modalDanger">Contact Us</button>

    <!-- Modal Danger -->
    <div class="modal fade modal-hmcf modal-primary" id="modalDanger" tabindex="-1" role="dialog" aria-labelledby="modalDangerLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times-circle-o"></i></button>
            <div class="row">
              <div class="col-sm-4">
                <i class="fa fa-comments icon"></i>
              </div>
              <div class="col-sm-8">
                <h4 class="modal-title" id="modalDangerLabel">Drop us a line</h4>
                <p>Our support team are available through email and twitter, simply contact us from below or tweet to <a href="https://twitter.com/tjebbelievens" target="_blank">@tjebbelievens</a> or <a href="https://www.twitter.com/icedesign" target="_blank">@icedesign</a>.</p>
                <address>
                  <strong>Tjebbe Lievens</strong><br>
                  Schransdijk 7A<br>
                  2440 Geel (België)<br>
                  <abbr title="Phone">P:</abbr> +32 (0) 475 37 55 57
                </address>
              </div>
            </div>
          </div>
          <form action="/contact" method="POST" class="form-contact" role="form">
            <div class="modal-body">
              <div class="form-group col-half">
                <input type="text" class="form-control" name="email" placeholder="Your Email" required>
                <i class="fa fa-envelope-o"></i>
              </div>
              <div class="form-group col-half">
                <input type="text" class="form-control" name="name" placeholder="Your Name" required>
                <i class="fa fa-smile-o"></i>
              </div>
              <div class="clearfix"></div>
              <div class="form-group">
                <textarea class="form-control message" rows="5" name="message" placeholder="Your Message" required></textarea>
                <i class="fa fa-file-text-o"></i>
              </div>
            </div>
            <div class="modal-footer">
              <span class="responce-server"></span>
              <button type="submit" class="btn">Send</button>
            </div>
          </form>
        </div>
      </div>
    </div> 



    <script src="{{ URL::asset('js/jquery.js') }}"></script>
    <script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('js/jquery.fitvids.js') }}"></script>
    <script src="{{ URL::asset('js/jquery.scrollUp.min.js') }}"></script>
    <script src="{{ URL::asset('js/jquery.singlePageNav.min.js') }}"></script>
    <script src="{{ URL::asset('js/jquery.cbpFWSlider.min.js') }}"></script>
    <script src="{{ URL::asset('js/script-home.js') }}"></script>
    <script src="{{ URL::asset('js/script.js') }}"></script>
  </body>
</html>
